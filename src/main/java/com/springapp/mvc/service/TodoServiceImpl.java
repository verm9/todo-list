package com.springapp.mvc.service;

import com.springapp.mvc.dao.TodoDao;
import com.springapp.mvc.model.Todo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by nono on 16.05.2016.
 */

@Service
public class TodoServiceImpl implements TodoService {
    private TodoDao todoDao;

    public void setTodoDao(TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    @Override
    @Transactional
    public void addTodo(Todo todo) {
        this.todoDao.addTodo(todo);
    }

    @Override
    @Transactional
    public void updateTodo(Todo todo) {
        this.todoDao.updateTodo(todo);
    }

    @Override
    @Transactional
    public void removeTodo(int id) {
        this.todoDao.removeTodo(id);
    }

    @Override
    @Transactional
    public Todo getTodoById(int id) {
        return this.todoDao.getTodoById(id);
    }

    @Override
    @Transactional
    public List<Todo> listTodos() {
        return this.todoDao.listTodos();
    }
}
