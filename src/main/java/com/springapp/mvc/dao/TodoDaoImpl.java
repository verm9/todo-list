package com.springapp.mvc.dao;

import com.springapp.mvc.model.Todo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by nono on 16.05.2016.
 */

@Repository
public class TodoDaoImpl implements TodoDao {
    private static final Logger logger = LoggerFactory.getLogger(TodoDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addTodo(Todo todo) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(todo);
        logger.info("Todo saved. Todo details: " + todo);
    }

    @Override
    public void updateTodo(Todo todo) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(todo);
        logger.info("Todo updated. Todo details: " + todo);
    }

    @Override
    public void removeTodo(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Todo todo = (Todo) session.load(Todo.class, new Integer(id));
        if (todo!= null) {
            session.delete(todo);
        }
        logger.info("Todo removed. Todo details: " + todo);
    }

    @Override
    public Todo getTodoById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Todo todo = (Todo) session.load(Todo.class, new Integer(id));
        logger.info("Todo loaded. Todo details: " + todo);
        return todo;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Todo> listTodos() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Todo> todos = session.createQuery("from Todo").list();

        for (Todo t : todos)
            logger.info("Todo list: " + t);

        return todos;
    }
}
