package com.springapp.mvc.dao;

import com.springapp.mvc.model.Todo;

import java.util.List;

/**
 * Created by nono on 16.05.2016.
 */
public interface TodoDao {
    public void addTodo(Todo todo);
    public void updateTodo(Todo todo);
    public void removeTodo(int id);
    public Todo getTodoById(int id);
    public List<Todo> listTodos();
}
