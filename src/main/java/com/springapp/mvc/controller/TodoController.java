package com.springapp.mvc.controller;

import com.springapp.mvc.model.Todo;
import com.springapp.mvc.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Iterator;
import java.util.List;

/**
 * Created by nono on 16.05.2016.
 */
@Controller
public class TodoController {
    private TodoService todoService;

    @Autowired(required = true)
    @Qualifier(value="todoService")
    public void setTodoService(TodoService todoService) {
        this.todoService = todoService;
    }

    @RequestMapping(value = "todos", method = RequestMethod.GET)
    public String listTodos(Model model) {
        model.addAttribute("todo", new Todo());
        model.addAttribute("listTodos", this.todoService.listTodos());

        return "todos";
    }

    @RequestMapping(value = "todos/{status}", method = RequestMethod.GET)
    public String filteredTodos(@PathVariable("status") String status, Model model) {
        model.addAttribute("todo", new Todo());
        List<Todo> todos = this.todoService.listTodos();
        Iterator<Todo> it = todos.iterator();
        while(it.hasNext()) {
            Todo t = it.next();
            if (status.equals("expecting")) {
                if (t.getStatus() == 1)
                    it.remove();
            }
            else if (status.equals("done")) {
                if (t.getStatus() != 1)
                    it.remove();
            }
        }

        model.addAttribute("listTodos", todos);

        return "todos";
    }

    @RequestMapping(value = "/todos/add", method = RequestMethod.POST)
    public String addTodo(@ModelAttribute("Todo") Todo todo) {
        if (todo.getId() == 0)
            this.todoService.addTodo(todo);
        else
            this.todoService.updateTodo(todo);

        return "redirect:/todos";
    }

    @RequestMapping(value = "/remove/{id}")
    public String removeTodo(@PathVariable("id") int id) {
        this.todoService.removeTodo(id);

        return "redirect:/todos";
    }

    @RequestMapping(value = "/edit/{id}")
      public String editTodo(@PathVariable("id") int id, Model model) {
        model.addAttribute("todo", this.todoService.getTodoById(id));
        model.addAttribute("listTodos", this.todoService.listTodos());

        return "todos";
    }

    @RequestMapping(value = "/done/{id}")
    public String doneTodo(@PathVariable("id") int id, Model model) {
        Todo todo = this.todoService.getTodoById(id);
        todo.setStatus(1);
        this.todoService.updateTodo(todo);

        return "redirect:/todos";
    }

    @RequestMapping("tododata/{id")
    public String todoData(@PathVariable("id") int id, Model model) {
        model.addAttribute("todo", this.todoService.getTodoById(id));

        return "tododata";
    }
}
