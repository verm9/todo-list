<%--
  Created by IntelliJ IDEA.
  User: nono
  Date: 16.05.2016
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>TODOs page</title>


</head>
<body>

<a href="../../index.jsp">Back to main page</a>
<br/>
<br/>

<h1>TODO list</h1>
Filter:
<a href="<c:url value="/todos"/>">Show all</a> |
<a href="<c:url value='/todos/expecting/'/>">Show uncompleted</a> |
<a href="<c:url value='/todos/done/'/>">Show done</a><br><br>
<c:if test="${!empty listTodos}">
  <table>
    <tr>
      <th width="100">ID</th>
      <th width="200">Todo</th>
      <th width="120">Status</th>
      <th width="100">Done</th>
      <th width="60">Edit</th>
      <th width="60">Delete</th>
    </tr>
    <c:forEach items = "${listTodos}" var="todo">
    <tr>
      <td>${todo.id}</td>
      <td>${todo.todo}</td>
      <td style="padding-left:20px;"><c:choose><c:when test="${todo.status == 1}">DONE</c:when><c:otherwise>expecting</c:otherwise></c:choose></td>
      <!-- pleeease. do not CSRF. -->
      <td><a href="<c:url value='/done/${todo.id}'/>">Done</a></td>
      <td><a href="<c:url value='/edit/${todo.id}'/>">Edit</a></td>
      <td><a href="<c:url value='/remove/${todo.id}'/>">Remove</a></td>
    </tr>
    </c:forEach>
  </table>
</c:if>



<c:if test="${!empty todo.todo}">
  <h1>Edit TODO</h1>
</c:if>
<c:if test="${empty todo.todo}">
  <h1>Add a TODO</h1>
</c:if>

<c:url var="addAction" value="/todos/add"/>
<form:form action="${addAction}" commandName="todo">
  <table>
    <c:if test="${!empty todo.todo}">
      <tr>
        <td>
          <form:label path="id">
            <spring:message text="ID"/>
          </form:label>
        </td>
        <td>
          <form:input path="id" readonly="true" size="8" disabled="true"/>
          <form:hidden path="id"/>
        </td>
      </tr>
    </c:if>
    <tr>
      <td>
        <form:label path="todo">
          <spring:message text="TODO"/>
        </form:label>
      </td>
      <td>
        <form:input path="todo"/>
      </td>
    </tr>
    <form:hidden path="status"/>
    </tr>
    <tr>
      <td colspan="2">
        <c:if test="${!empty todo.todo}">
          <input type="submit"
                 value="<spring:message text="Edit todo"/>"/>
        </c:if>
        <c:if test="${empty todo.todo}">
          <input type="submit"
                 value="<spring:message text="Add todo"/>"/>
        </c:if>
      </td>
    </tr>
  </table>
</form:form>

</body>
</html>
