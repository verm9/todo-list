<%--
  Created by IntelliJ IDEA.
  User: nono
  Date: 16.05.2016
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>TODO details</title>
</head>
<body>
<table class="tg">
  <tr>
    <th width="80">ID</th>
    <th width="120">TODO</th>
    <th width="60">Status</th>
  </tr>
  <tr>
    <td>${todo.id}</td>
    <td>${todo.todo}</td>
    <td>${todo.status}</td>
  </tr>
</table>
</body>
</html>
